use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response, http::HeaderValue};
use aws_config::meta::region::RegionProviderChain;
use aws_sdk_dynamodb::{Client, types::AttributeValue};
use serde_json::json;
use std::str::FromStr;
/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Initialize the DynamoDB client
    let region_provider = RegionProviderChain::default_provider().or_else("us-west-2");
    let config = aws_config::from_env().region(region_provider).load().await;
    let client = Client::new(&config);

    // Extract party_size parameter from request
    let query_params = event.query_string_parameters();
    let party_size_str = query_params.first("party_size").unwrap_or_default();
    let party_size = i32::from_str(&party_size_str).unwrap_or(0);

    // Query a DynamoDB table
    let resp = client.query()
        .table_name("PartyGames")
        .key_condition_expression("PartySize = :party_size")
        .expression_attribute_values(":party_size", AttributeValue::N(party_size.to_string()))
        .send()
        .await;

        match resp {
            Ok(output) => {
                // Process query results
                let items = output.items.unwrap_or_default();
                if items.is_empty() {
                    // If no items are found, an error message is returned
                    let body = json!({ "party_size": party_size, "error": "Cannot find any games!" }).to_string();
                    Ok(Response::builder()
                        .status(200)
                        .header("Content-Type", "application/json")
                        .body(Body::from(body))
                        .expect("Failed to render response"))
                } else {
                    let games = items.iter().flat_map(|item| {
                        // Try to extract GameList from each item, which is a collection of strings
                        match item.get("GameList").and_then(|v| v.as_ss().ok()) {
                            Some(ss) => ss.iter().cloned().collect::<Vec<String>>(), // If found successfully and is a collection of strings, clone each string
                            None => vec![], // If GameList is not found or is not a collection of strings, returns an empty Vec
                        }
                    }).collect::<Vec<String>>(); // Combine all games into one Vec
        
                    // Create response
                    let body = json!({ "games": games }).to_string();
                    Ok(Response::builder()
                        .status(200)
                        .header("Content-Type", "application/json")
                        .body(Body::from(body))
                        .expect("Failed to render response"))
                }
            },
            Err(_) => {
                // Handling when query errors occur
                let body = json!({ "error": "Failed to query DynamoDB" }).to_string();
                Ok(Response::builder()
                    .status(500)
                    .header("Content-Type", "application/json")
                    .body(Body::from(body))
                    .expect("Failed to render response"))
            }
        }
    }        

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await;
    Ok(())
}
