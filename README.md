# Serverless Rust Microservice

This Rust Microservice is a service called party game selector on AWS. It can return the corresponding game list based on the input party size (from 2 to 8) in the database. It will also return an error message if the query failed.

## 1. Build Dynamo Table
We can design a DynamoDB table with two attributes:

- PartySize: A number (2 to 8) representing the number of people in the party.
- GameList: A list of strings containing recommended games.
![screenshot of database](https://gitlab.com/estrellas939/week5proj/-/raw/main/pic/database.png?inline=false)

## 2. Build a New Rust Directory

```
cargo new party_game_selector
cd party_game_selector
```

## 3. Edit Config File

First, add the dependency to `Cargo.toml` file
```
[dependencies]
aws-config = "1.1.6" 
aws-sdk-dynamodb = "1.15.0" 
lambda_runtime = "0.10.0" 
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
log = "0.4.0"
simple_logger = "1.0"
tokio = { version = "1", features = ["full"] }
lambda_http = "0.10.0"
```

## 4. Build Lambda Function

Next, write the Lambda function logic in `main.rs`.

## 5. Local Test

After building the function logic, do a local test to see if the result is good:
```
cargo lambda watch
```
Open another terminal and type:
```
curl "http://localhost:9000/?party_size=5"
```
We can see the result is correct:
```
{"games":["League of Legends"]}
```
We can also adjust `party_size` to other numbers to test other conditions.

## 6. Deploy to AWS Lambda

Once the local test is passed, compile the function.
```
cargo lambda build --release
```
And we can see the result:
```
Compiling party_game_selector v0.1.0 (/Users/wanghaotian/party_game_selector)
    Finished release [optimized] target(s) in 56.55s
```
Then, deploy to AWS Lambda:
```
cargo lambda deploy
```
Terminal output:
```
🔍 function arn: arn:aws:lambda:us-east-1:568937614068:function:party_game_selector:2
```

## 7. Authorize IAM to DynamoDB Permission 

Then, we need to login the AWS account and go into IAM panel. We should create a new role with AWS Lambda as use case. Add permissions to the role so that it can visit DynamoDB database.
![screenshot of IAM role](https://gitlab.com/estrellas939/week5proj/-/raw/main/pic/iam-role.png?inline=false)

## 8. Add API to AWS Lambda

After authorizing permission, we need to create a trigger and here, I choose API gateway.
Just simply create a HTTP API and use `POST` as the method, create a stage and deploy manually.
![screenshot of api](https://gitlab.com/estrellas939/week5proj/-/raw/main/pic/api.png?inline=false)

## 9. Config the Lambda Function

Go into the AWS Lambda panel. We should see the lambda function we deploy just now. Config it with the role we created in step 7. Also connect the function with the API gateway.
![screenshot of lambda function](https://gitlab.com/estrellas939/week5proj/-/raw/main/pic/lambda.png?inline=false)

## 9. AWS Lambda Test

Select `Test` sub-panel, create a new test case with the following JSON file format:
```
{
  "httpMethod": "POST",
  "headers": {
    "Content-Type": "application/json"
  },
  "path": "/your/path",
  "queryStringParameters": {
    "party_size": "7"
  },
  "requestContext": {
    "identity": {
      "sourceIp": "127.0.0.1"
    },
    "httpMethod": "POST",
    "path": "/your/path"
  }
}
```
We can see the Lambda function pass the test case.
![screenshot of the result](https://gitlab.com/estrellas939/week5proj/-/raw/main/pic/test.png?inline=false)
As we can see from the result: when the party size is 7, we can get the output as
```
"body": "{\"games\":[\"Super Smash Bros Ultimate\"]}"
```

## 10. API Test

We can also test our function via API.
Just type the following command as the terminal input:
```
curl -X POST "https://yecdro8wbk.execute-api.us-east-1.amazonaws.com/default/party_game_selector?party_size=5" -H "Content-Type: application/json"
```
And we should see the following terminal output:
```
{"games":["League of Legends"]}
```
We can change the number of `party_size` anytime to see different results.
